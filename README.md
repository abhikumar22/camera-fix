# TakeSnapshot

## Purpose

Takesnapshot module is designed to ensure resuability of Camera feature across multiple web as well as mobile application.
As per the earlier implementation there is no such module which can handle to Capture Picture in both mobile as well as web apps.
As of now, the input field is taken into consideration for this implementation, for mobile app it will open default camera app to capture picture but in desktop it will open file explorer to select a image file which is not correct.

So, this module will open a camera popup over your display to capture picture and the mobile app the behaviour remains the same what it was earlier. Also it will check in desktop that if camera enabled or available, of not then it will simply

## Requirements
1. User can do the following:
	a) Capture pictures in mobile and web apps
	b) View the preview of captured scenes
	c) Retake the images as per their choice

## Props

Prop Name | isMandatory | Type | Default Value | Description
--- | --- | --- | --- |--- 
activeCameraIcon | No | string | CameraIcon | Active Camera Icon path which is shown
disabledCameraIcon | No | string | CameraIconDisabled | Disabled Camera Icon path which is shown
inputName | No | string | 'CAMERA' | Targert value key name for the result object
popupContainerClass | No | string |  | Additional Css style classname for overlay popup styling
cameraIconStyleClass | No | string |  | Additional Css style classname for Camera Icon styling
idForHtml | No | string | 'attachment--camera' | Embedd External label to the camera click
handleAttachment | Yes | function |  | The target value is supplied to this function as parameter event
setCameraPopupToDiv | Yes | function |  | Camera component is supplied as funtion parameter which should be set in your Div

#### Note :- setCameraPopupToDiv will supply a Camera component which should be set in a div(Div's Parent Div should not be position absoulte as the camera popup style have position fixed)

# Usage:

- Install ‘webmodules’ npm package  
#### npm install webmodules

- Import TakeSnapshot module   
#### import {TakeSnapshot} from 'webmodules';

- Using the module
#### Using the module
	<TakeSnapshot
		idForHtml={'attachment--camera'}
		handleAttachment={handleAttachment}
		setCameraPopupToDiv={setCameraToDiv}
		disabledCameraIcon={disabledCameraAttachmentIcon}
		activeCameraIcon={cameraAttachmentIcon}
		inputName={'CAMERA'}
		cameraIconStyleClass={'attachment_option bg_white'}
	/>

- Steps for integrate

    - Initialise a variable for setting div in your Component and declare a funtion to set the state<br />
      this.state = {
    	cameraComponent: null
       };
	- Declare it in any div( Parent div should not be position absolute )
		{cameraComponent &&
          cameraComponent
        }
	- Callback function to set the state when Desktop camera activates
		  setCameraToDiv = (child) => {
			this.setState({ cameraComponent: child })
		  }






